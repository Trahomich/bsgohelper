package cmshelper

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"net/http/cookiejar"
	"bytes"
)

type State struct {
	Hashed_number string `json:"hashed_number"`
	Create_at     string `json:"status"`
}

type Status struct {
	Title   string `json:"title"`
	Version string `json:"version"`
	State   State  `json:"state"`
}

func Login(host, user, password string) ([]*http.Cookie, error) {
	// Возвращает залогиненого http клиента

	res, err := http.PostForm(fmt.Sprintf(host+"/auth/jwt/login"), url.Values{
		"username": {user},
		"password": {password},
	})
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	if res.StatusCode != 204 {
		fmt.Println("Unexpected status code", res.StatusCode)
	}

	cookies := res.Cookies()

	defer res.Body.Close()
	return cookies, err
}

func GetStatus(host string) (string, error) {
	var staus = new(Status)
	req, err := http.NewRequest("GET", host, nil)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	defer res.Body.Close()

	jerr := json.Unmarshal(body, staus)
	if jerr != nil {
		return "", jerr
	}
	return staus.State.Hashed_number, nil
}

///////////   message ////////////
type message struct {
	Type         string `json:"type"`
	Source       string `json:"source"`
	Message      string `json:"message"`
	Id           string `json:"id"`
	Hash_message string `json:"hash_message"`
	Created_at   string `json:"created_at"`
}

func PushMessage(host string, cookies []*http.Cookie, Type string, Source string, Message string) (id string, err error) {

	data := `{
		"type" :      "` + Type + `",
		"source" :    "` + Source + `",
		"message" :   ` + Message + `
	}`

	jar, err := cookiejar.New(nil)
	if err != nil {
		return "", err
	}

	client := &http.Client{
		Jar: jar,
	}

	req, err := http.NewRequest("POST", host+"/message", bytes.NewBuffer([]byte(data)))
	if err != nil {
		return "", err
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	client.Jar.SetCookies(req.URL, cookies)

	// Отправляем запрос
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Ошибка при отправке запроса: ", err)
		return "", err
	}

	// Читаем ответ
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)

	var m map[string]interface{}
	decodeerr := json.Unmarshal(body, &m)
	if decodeerr != nil {
		return "", decodeerr
	}

	id, ok := m["Id"].(string)
	if !ok {
		return "", nil
	} else {
		return id, nil
	}
	
}

//////////   //////////////