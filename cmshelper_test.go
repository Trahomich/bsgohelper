package cmshelper_test

import (
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"testing"

	"gitlab.com/Trahomich/cmshelper"
)

//////////////////////////  testing //////////////////////
func TestLogin(t *testing.T) {
	cookies, err := cmshelper.Login("https://cms-stage.brainysoft.ru", "user@example.com", "string")
	if err != nil {
		log.Fatal(err)
	}

	jar, err := cookiejar.New(nil)
	if err != nil {
		log.Fatal(err)
	}

	client := &http.Client{
		Jar: jar,
	}

	req, err := http.NewRequest("GET", "https://cms-stage.brainysoft.ru/api/v1/clusters/", nil)
	if err != nil {
		t.Errorf("Client create err")
	}

	req.Header.Add("Content-Type", "application/json")
	client.Jar.SetCookies(req.URL, cookies)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Http read error")
	}
	defer res.Body.Close()

	t.Logf("Status %d", res.StatusCode)

	b, err := io.ReadAll(res.Body)

	if err != nil {
		t.Errorf("Read body error %s", b)
	}
	t.Logf("Body %s", b)

}

func TestGetStatus(t *testing.T) {
	
	state, err := cmshelper.GetStatus("https://cms-stage.brainysoft.ru")
	if err != nil {
		t.Errorf("Get status err %s", err.Error())
	}

	t.Logf("test state is %s", state)

	if state == "" {
		t.Errorf("State is nil")
	}
}
